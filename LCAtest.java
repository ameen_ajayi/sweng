

import static org.junit.jupiter.api.Assertions.assertEquals;


import org.junit.Test;



public class LCAtest {
    @Test

    public void testlca()
	{
		BinaryTree tree = new BinaryTree();
		tree.root = new Node(1);
		tree.root.left = new Node(2);
		tree.root.right = new Node(3);
		tree.root.left.left = new Node(4);
		tree.root.left.right = new Node(5);
		tree.root.right.left = new Node(6);
		tree.root.right.right = new Node(7);

        /*

                    1
             _______|_______
            2               3
         ___|___         ___|___
        4       5       6       7
        */

        assertEquals(tree.findLCA(4, 5), 2);
        assertEquals(tree.findLCA(6, 7), 3);
        assertEquals(tree.findLCA(2, 3), 1);
        assertEquals(tree.findLCA(3, 2), 1);
        assertEquals(tree.findLCA(2, 7), 1);
        assertEquals(tree.findLCA(3, 7), 3);
        assertEquals(tree.findLCA(4, 7), 1);
	}
}
